import logger from './utils/logger.js';
import config from './utils/config.js';
import { Sequelize } from 'sequelize';

import testTable from './models/testModel.js'

let sequelize;

let tabla; // podria ser una instancia de uno de los modelos de sequelize. Ej: sequelize.models.testTable;

// Se crea el pool de conexiones (dicha acción incluye la conexión a la BD)
// Dejamos comentada la asignación de algunos atributos por si es necesario usarlas.
export const instanciarSequelize = async () => {
  try {
    const sequelizeInstance = new Sequelize(
      config.BD_SERVICE || config.BD_SCHEMA,
      config.BD_USER,
      config.BD_PASSWORD,
      {
        host: config.BD_HOST,
        port: config.BD_PORT,
        dialect: config.BD_DIALECT,
        pool: {
          min: config.BD_POOL_MIN,
          max: config.BD_POOL_MAX
        },
        logging: false
        }
    )
    
    // Instanciar todos los modelos que se necesite...
    testTable(sequelizeInstance)
    
    // Sincronizo el modelo para que cree la tabla, y cargo un par de ejemplos de prueba. Solo para fines de desarrollo y comodidad!
    await sequelizeInstance.models.testTable.sync({ force: true });
    await sequelizeInstance.models.testTable.create({ CAMPO_CLAVE: "ejemplo1" });
    await sequelizeInstance.models.testTable.create({ CAMPO_CLAVE: "ejemplo2" });

    logger.info('Sequelize instanciado exitosamente');
    
    sequelize = sequelizeInstance;

    return;
  } catch (err) {
    logger.error(`Error al instanciar Sequelize: ${err.message}`);
    throw(err); // Arrojo el error para que el invocador de la función lo pueda atrapar
  }
};

// Cierra el pool de conexiones (previo chequeo de si está definido)
export const cerrarPool = async () => {
  // Si sequelize se encuentra instanciado, intento cerrar su pool de conexiones
  if (sequelize) {
    try {
      await sequelize.close();
      logger.info('Pool de Conexiones cerrado exitosamente');
    } catch (err) {
      logger.error(`Ocurrió un error al intentar cerrar el pool: ${err.message}`);
      throw err;
    }
  } else {
    logger.info('Se omite cierre del pool debido a que no está definido');
  }
};

// Hace un ping contra la BD
export const test = async () => {
  try {
    await sequelize.authenticate();
    logger.info('Ejecuto Ping');
    return;
  } catch (err) {
    logger.error(`Error al realizar test en la BD: ${err.message}`);
    throw err;
  }
};

export const esTimeout = (err) => ['NJS-123', 'NJS-040', 'DPI-1067', 'DPI-1080'].some((code) => code == err?.code);

// Definir queries debajo de esta linea:

export const obtenerEjemplos = async () => {
  return await sequelize.models.testTable.findAll({
    attributes: [
      ["CAMPO_CLAVE", "campoClave"],
      ["CAMPO_ALFANUMERICO", "campoAlfanumerico"],
      ["CAMPO_ENTERO", "campoEntero"],
      ["CAMPO_DECIMAL", "campoDecimal"],
      ["CAMPO_FECHA", "campoFecha"],
      ["CAMPO_TIMESTAMP", "campoTimestamp"]
    ]
  });
};

export const obtenerEjemplo = async (CAMPO_CLAVE) => {
  return await sequelize.models.testTable.findOne({ where: { CAMPO_CLAVE } });
}

export const almacenarEjemplo = async (data) => {
  const { campoClave, campoAlfanumerico, campoEntero, campoDecimal, campoFecha, campoTimeStamp } = data;
  
  return await sequelize.models.testTable.create({
    CAMPO_CLAVE: campoClave,
    CAMPO_ALFANUMERICO: campoAlfanumerico,
    CAMPO_ENTERO: campoEntero,
    CAMPO_DECIMAL: campoDecimal,
    CAMPO_FECHA: campoFecha,
    CAMPO_TIMESTAMP: campoTimeStamp
  })
}

export const eliminarTabla = async () => {
  return await sequelize.models.testTable.drop();
};

export const limpiarTabla = async () => {
  return await sequelize.models.testTable.truncate();
};

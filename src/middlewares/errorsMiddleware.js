import { ErrorHttp, toErrorHttp } from '../utils/errors.js';

export const notFound = (req, res, next) => {
  return next(new ErrorHttp('ERROR_RECURSO_INEXISTENTE', `Recurso Inexistente o método no permitido- ${req.originalUrl}`));
};

// Atrapar errores propagados
// eslint-disable-next-line no-unused-vars
export const handler = (err, req, res, next) => {
  const errorHttp = toErrorHttp(err);
  res.status(errorHttp.codigoHttp).json(errorHttp.respuesta());
};

export const allowedMethods = (...allowedMethods) => (req, res, next) => {
  const methodIsAllowed = allowedMethods.some(method => method.toUpperCase() === req.route.method);
  
  if (!methodIsAllowed)
    next(new ErrorHttp('ERROR_METODO_NO_PERMITIDO'));
  else
    next();
};
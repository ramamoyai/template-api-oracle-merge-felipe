import { isCelebrateError } from 'celebrate';
import { celebrateErrorHttpMapper, ErrorHttp } from '../utils/errors.js';

export const celebrateErrorsMiddleware = (err, req, res, next) => {
  if (!isCelebrateError(err))
    return next(err);

  const key = err.details.keys().next().value; //Si hay un error de celebrate al menos hay un error.
  next(new ErrorHttp(celebrateErrorHttpMapper[key], err.details.get(key).message));
};
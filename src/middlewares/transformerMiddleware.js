import mung from 'express-mung';
import { v4 as uuidv4 } from 'uuid';
import httpContext from 'express-http-context';

export const clonarResBody = mung.json(
  (body, req, res) => {
    res.locals.body = body;
    return body;
  },
  { mungError: true }
);

export const generarUuid = (req, res, next) => {
  httpContext.set('uuid', uuidv4());
  next();
};

/**
 * Middleware que propaga los headers internos.
 */
export const setHeaders = (req, res, next) => {
  res.append('x-cliente-id', req.header('x-cliente-id') || '');
  res.append('x-transaccion-id', req.header('x-transaccion-id') || '');
  next();
};

/**
 * Middleware que setea el id de trans. de la API por request
 */
export const setApiTransactionIDHeader = (req, res, next) => {
  res.append('x-api-transaccion-id', httpContext.get('uuid'));
  next();
};
import morgan from 'morgan';
import { stream } from '../utils/logger.js';
import config from '../utils/config.js';

morgan.token('req-headers', (req) => JSON.stringify(req.headers));
morgan.token('req-body', (req) => JSON.stringify(req.body));
morgan.token('res-body', (req, res) => JSON.stringify(res.locals.body));

const loggerHttp = morgan(config.LOG_HTTP_FORMAT, {
  stream
});

export default loggerHttp;

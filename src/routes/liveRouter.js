import { Router } from 'express';
import { getLiveController } from '../controllers/liveController.js';

const LiveRouter = Router();

LiveRouter.get('/', getLiveController);

export default LiveRouter;
import { Router } from 'express';
import { guardarNuevoEjemploController, obtenerEjemplosController, obtenerEjemploController } from '../controllers/ejemploController.js';
import { validarPostEjemplo, validarGetEjemplo } from '../utils/validator.js';

const EjemploRouter = Router();

EjemploRouter.route('/').get(obtenerEjemplosController).post(validarPostEjemplo, guardarNuevoEjemploController); // Temgo problemas con la validacion del timestamp con Joi
EjemploRouter.route('/:campoClave').get(validarGetEjemplo, obtenerEjemploController);

export default EjemploRouter;

import { Router } from 'express';
import { getReadyController } from '../controllers/readyController.js';

const ReadyRouter = Router();

ReadyRouter.get('/', getReadyController);

export default ReadyRouter;

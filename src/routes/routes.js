import live from './liveRouter.js';
import ready from './readyRouter.js';
//import acciones from './acciones.js';
import apiDocs from './api-docsRouter.js';
// En caso de necesitar validar existencia de headers x-cliente-id y x-transaccion-id descomentar la siguiente linea
// import { idHeadersValidationMiddleware } from '../utils/validator.js';
import { Router } from 'express';
import EjemploRouter from './ejemploRouter.js';

const router = Router();

// En caso de necesitar validar existencia de headers x-cliente-id y x-transaccion-id descomentar la siguiente linea
// router.use('/', idHeadersValidationMiddleware);

// TODO: Agregar las rutas que necesite tu app ¡Y acordate de borrar el ejemplo!
router.use('/ejemplo', EjemploRouter);

// Rutas complementarias y comunes a todas las apis
router.use('/live', live);
router.use('/ready', ready);
router.use('/api-docs', apiDocs);

export default router;

import Router from 'express';
import { prepareOpenAPI, serveOpenAPI, setupOpenAPI } from '../controllers/api-docsController.js';

const APIDocsRouter = Router();

APIDocsRouter.use('/', serveOpenAPI);

APIDocsRouter.get('/', prepareOpenAPI, setupOpenAPI);

export default APIDocsRouter;
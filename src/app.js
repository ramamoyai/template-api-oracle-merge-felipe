import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import loggerHttp from './middlewares/loggerHttpMiddleware.js';
import * as transformer from './middlewares/transformerMiddleware.js';
import httpContext from 'express-http-context';
import * as errores from './middlewares/errorsMiddleware.js';
import routes from './routes/routes.js';
import { celebrateErrorsMiddleware } from './middlewares/celebrateMiddleware.js';
import config from './utils/config.js';

const app = express();

app.use(express.json()); // Parsea formato JSON y genera el objeto body
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
); // Define Headers de http de seguridad
app.use(cors()); // Define Headers de http para habilitar requests cruzados
app.use(loggerHttp); // Habilita el log http
app.use(httpContext.middleware); // Crea un contexto para cada request
app.use(transformer.clonarResBody); // Duplica el body de la respuesta para que le llegue al loggerHttp
app.use(transformer.generarUuid); // Genera uuid interno para trazabilidad interna
app.use(transformer.setApiTransactionIDHeader); // Setea el header de transaccion de la api

// TODO: definir la ruta base en default.env mediante APP_NAME
app.use(`/${config.APP_NAME}/v1`, routes); // Defino las rutas de la aplicación

// Middleware de error --> Esto siempre va al final
app.use(celebrateErrorsMiddleware); // Middleware para catchear los errores de Celebrate
app.use(errores.notFound); // Controla y dispara el 404
app.use(errores.handler); // Manejo de errores

export default app;

import { DataTypes } from 'sequelize';

const testTable = sequelize => {
  return sequelize.define('testTable', {
    CAMPO_CLAVE: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    CAMPO_ALFANUMERICO: {
      type: DataTypes.STRING,
    },
    CAMPO_ENTERO: {
      type: DataTypes.INTEGER
    },
    CAMPO_DECIMAL: {
      type: DataTypes.FLOAT
    },
    CAMPO_FECHA: {
      type: DataTypes.DATE // o DATEONLY
    },
    CAMPO_TIMESTAMP: {
      type: DataTypes.TIME, // oracle no permite TIME, dice que es un invalid datatype
    }
  }, {
    tableName: "test_table",
    timestamps: false
  });
}

export default testTable;
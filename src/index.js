import config from './utils/config.js';
import logger from './utils/logger.js';
import app from './app.js';

import { instanciarSequelize, cerrarPool } from './database.js';

const cerrarServidor = () => {
  return new Promise((resolve, reject) => {
    servidor.close((err) => {
      if (err) {
        logger.error('Error al cerrar el servidor');
        reject(err);
      } else {
        logger.info('Servidor cerrado exitosamente');
        resolve();
      }
    });
  });
};

const salir = async (exitCode) => {
  logger.info('Finalizando Servidor...');

  try {
    await cerrarPool();
    await cerrarServidor();
  } catch (err) {
    logger.error(`Error al salir: ${err}`);
  } finally {
    process.exit(exitCode || process.exitCode || 0);
  }
};

const servidor = app
  .listen(config.APP_PORT, () => {
    logger.info(`Servidor escuchando en el puerto ${config.APP_PORT}`);
  })
  .on('error', (err) => {
    logger.error(`El servidor no pudo iniciar. ${err.message}`);
    salir(1);
  });


instanciarSequelize().catch(() => salir(1));


process.on('SIGINT', async () => {
  logger.info('Señal SIGINT (ctrl-c).');
  await salir();
});

process.on('SIGTERM', async () => {
  logger.info('Señal SIGTERM (stop).');
  await salir();
});

process.on('exit', (code) => {
  logger.info(`Código de Salida: ${code}`);
});

process.on('unhandledRejection', (err) => {
  logger.error(`Rechazo de promesa no atrapado. ${err.message}`);
});

process.on('uncaughtException', (err) => {
  logger.error(`Excepción no atrapada. ${err.message}`);
});

process.on('error', (err) => {
  logger.error(`Error no atrapado. ${err.message}`);
});

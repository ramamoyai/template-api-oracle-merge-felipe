import logger from './logger.js';

const CODIGOS_HTTP_DEFAULT = {
  400: 'ERROR_REQUERIMIENTO_INVALIDO',
  404: 'ERROR_RECURSO_INEXISTENTE',
  405: 'ERROR_METODO_NO_PERMITIDO',
  500: 'ERROR_INTERNO',
  503: 'ERROR_SERVICIO_NO_DISPONIBLE',
  504: 'ERROR_TIMEOUT_INTERNO',
};

// Errores comunes a todas las apis
const ERRORES = {
  ERROR_REQUERIMIENTO_INVALIDO: {
    codigo: 'ERROR_REQUERIMIENTO_INVALIDO',
    codigoHttp: 400,
    mensaje: 'El requerimiento es inválido',
  },
  ERROR_PARAMETROS: {
    codigo: 'ERROR_PARAMETROS',
    codigoHttp: 400,
    mensaje: 'Parámetros Inválidos',
  },
  ERROR_BODY: {
    codigo: 'ERROR_BODY',
    codigoHttp: 400,
    mensaje: 'Error en el cuerpo del mensaje',
  },
  ERROR_HEADERS: {
    codigo: 'ERROR_HEADERS',
    codigoHttp: 400,
    mensaje: 'Error en los headers',
  },
  ERROR_COOKIES: {
    codigo: 'ERROR_COOKIES',
    codigoHttp: 400,
    mensaje: 'Error en el header de las cookies',
  },
  ERROR_PATH_PARAMS: {
    codigo: 'ERROR_PATH_PARAMS',
    codigoHttp: 400,
    mensaje: 'Error en los parámetros de ruta',
  },
  ERROR_QUERY_PARAMS: {
    codigo: 'ERROR_QUERY_PARAMS',
    codigoHttp: 400,
    mensaje: 'Error en los query params',
  },
  ERROR_RECURSO_INEXISTENTE: {
    codigo: 'ERROR_RECURSO_INEXISTENTE',
    codigoHttp: 404,
    mensaje: 'Recurso Inexistente',
  },
  ERROR_METODO_NO_PERMITIDO: {
    codigo: 'ERROR_METODO_NO_PERMITIDO',
    codigoHttp: 405,
    mensaje: 'Método No Permitido',
  },
  ERROR_INTERNO: {
    codigo: 'ERROR_INTERNO',
    codigoHttp: 500,
    mensaje: 'Error Interno del Servidor',
  },
  ERROR_SERVICIO_NO_DISPONIBLE: {
    codigo: 'ERROR_SERVICIO_NO_DISPONIBLE',
    codigoHttp: 503,
    mensaje: 'Servicio No Disponible',
  },
  ERROR_TIMEOUT_INTERNO: {
    codigo: 'ERROR_TIMEOUT_INTERNO',
    codigoHttp: 504,
    mensaje: 'Error de Timeout Interno',
  },
  ERROR_TEST_BD: {
    codigo: 'ERROR_TEST_BD',
    codigoHttp: 500,
    mensaje: 'Error al realizar el test en la BD',
  },
  // Errores particulares de esta api
  // TODO: Los errores particulares de esta API deberían definirse acá abajo
  ERROR_OPERACION_BD: {
    codigo: 'ERROR_OPERACION_BD',
    codigoHttp: 500,
    mensaje: 'Error al ejecutar la operación en la base de datos',
  },
  ERROR_EJEMPLO_NO_ENCONTRADO: {
    codigo: 'ERROR_EJEMPLO_NO_ENCONTRADO',
    codigoHttp: 404,
    mensaje: 'Ejemplo no encontrado',
  },
};

export const celebrateErrorHttpMapper = {
  body: 'ERROR_BODY',
  cookies: 'ERROR_COOKIES',
  headers: 'ERROR_HEADERS',
  params: 'ERROR_PATH_PARAMS',
  query: 'ERROR_QUERY_PARAMS',
};

export class ErrorHttp extends Error {
  constructor(codigoInterno = 'ERROR_INTERNO', mensaje) {
    if (!ERRORES[codigoInterno]) {
      logger.warn(`El código de error interno ${codigoInterno} no existe. Cambio por ERROR_INTERNO`);
      codigoInterno = 'ERROR_INTERNO';
    }
    super(mensaje || ERRORES[codigoInterno].mensaje);
    this.codigoHttp = ERRORES[codigoInterno].codigoHttp;
    this.codigo = ERRORES[codigoInterno].codigo;
  }

  respuesta() {
    //Formateo la respuesta con code/message vez de codigo/descripcion para igualar a la api original
    return {
      codigo: this.codigo,
      mensaje: this.message,
    };
  }
}

export const toErrorHttp = (err) => {
  if (err instanceof ErrorHttp) {
    return err;
  } else {
    return new ErrorHttp(CODIGOS_HTTP_DEFAULT[`${err.statusCode}`]);
  }
};

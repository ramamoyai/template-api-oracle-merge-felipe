import { esTimeout } from '../database.js';
import { ErrorHttp } from './errors.js';
import logger from './logger.js';

export const catchDbError = (err, next) => {
  logger.error(err.message);
  if (esTimeout(err))
    //Timeout de la DB
    return next(new ErrorHttp('ERROR_TIMEOUT_INTERNO'));

  return next(new ErrorHttp('ERROR_OPERACION_BD'));
};

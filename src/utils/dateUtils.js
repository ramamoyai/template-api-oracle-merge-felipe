export const toLocaleISOString = (fecha) => {
  const pad = (number) => {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  };
  let signo = '';
  if (fecha.getTimezoneOffset() > 0) {
    signo = '-';
  } else {
    signo = '+';
  }
  const GMT = signo + pad((fecha.getTimezoneOffset() / 60).toFixed(2)).replace('.', ':');
  return `${fecha.getFullYear()}-${pad(fecha.getMonth() + 1)}-${pad(fecha.getDate())}T${pad(fecha.getHours())}:${pad(
    fecha.getMinutes()
  )}:${pad(fecha.getSeconds())}.${(fecha.getMilliseconds() / 1000).toFixed(3).slice(2, 5)}${GMT}`;
};

import { celebrate, Joi, Segments } from 'celebrate';
import messages from './messages.js';

const celebrateWithDefaultOptions = (
  schema,
  opts = {
    abortEarly: false, // que no aborte al encontrar un error
    messages, // lista de mensajes de error
  }
) => celebrate(schema, opts);

export const idHeadersValidationMiddleware = celebrateWithDefaultOptions({
  [Segments.HEADERS]: Joi.object({
    'x-cliente-id': Joi.string(),
    'x-transaccion-id': Joi.string(),
  }).unknown(), // descarto el resto de los headers
});

export const validarPostEjemplo = celebrateWithDefaultOptions({
  [Segments.BODY]: Joi.object({
    campoClave: Joi.string().required(),
    campoAlfanumerico: Joi.string().required(),
    campoEntero: Joi.number().integer().positive().required(),
    campoDecimal: Joi.number().precision(2).required(),
    campoFecha: Joi.date().iso().required(),
    campoTimeStamp: Joi.required(), // Tengo problemas con este validador. Creo que hau que utilizar una RegEx para lo que es DataType.TIME 
  }),
});

export const validarGetEjemplo = celebrateWithDefaultOptions({
  [Segments.PARAMS]: Joi.object({
    campoClave: Joi.string().required(),
  }),
});

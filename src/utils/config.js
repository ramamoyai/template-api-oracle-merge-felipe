import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';
import { Joi } from 'celebrate';
import messages from './messages.js';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

// Si estoy corriendo los test toma las variables de entorno correspondientes
if (process.env.NODE_ENV === 'test') {
  dotenv.config({ path: path.join(__dirname, '../../test.env') });
}

// Levanto si existe el archivo .env (para uso local). No pisa variables existentes.
dotenv.config();

// Levanto el archivo .env.default. Setea variables por defecto. No pisa variables existentes.
dotenv.config({ path: path.join(__dirname, '../../default.env') });

const processEnvSchema = Joi.object({
  //App Env vars
  APP_PORT: Joi.number().port().required(),
  APP_NAME: Joi.string().required(),

  //DB Env vars
  BD_HOST: Joi.string().hostname().required(),
  BD_PORT: Joi.number().port().required(),
  BD_USER: Joi.string().required(),
  BD_PASSWORD: Joi.string().required(),
  BD_SERVICE_NAME: Joi.string(),
  BD_SCHEMA: Joi.string(),
  BD_DIALECT: Joi.string().required(),
  BD_POOL_MAX: Joi.number().integer().positive().required(),
  BD_POOL_MIN: Joi.number().integer().positive().required(),
  BD_POOL_INCREMENT: Joi.number().integer().positive().required(),
  BD_POOL_QUEUE_TIMEOUT: Joi.number().integer().positive().required(),
  BD_POOL_DRAIN_TIME: Joi.number().integer().positive().required(),
  BD_QUERY_TIMEOUT: Joi.number().integer().positive().required(),

  //Log Env vars
  LOG_LEVEL: Joi.any().valid('error', 'warn', 'info', 'http', 'debug').required(),
  LOG_HTTP_FORMAT: Joi.string().required(),
  LOG_COLORIZE: Joi.boolean().required(),
  LOG_FILE_ENABLED: Joi.boolean().required(),
  LOG_FILE_NAME: Joi.string().required(),
  LOG_FILE_MAX_SIZE: Joi.number().integer().positive().required(),
  LOG_FILE_MAX_FILES: Joi.string().required(),
}).xor('BD_SCHEMA', 'BD_SERVICE_NAME');

// Opciones para el validate de Joi
const validationOptions = {
  abortEarly: false, // que no aborte al encontrar un error
  allowUnknown: true, // permite variables que se encuentren en el esquema
  stripUnknown: true, // elimina del objeto casteado las variables sobrantes
  messages, // lista de mensajes de error
};

let configObject;
try {
  const { error, value } = processEnvSchema.validate(process.env, validationOptions);

  if (error) {
    throw error;
  } else {
    configObject = value;
  }
} catch (err) {
  // eslint-disable-next-line no-console
  console.error(`Error en las variables de entorno: ${err.message}`);
  process.exit(1);
}

export default configObject;

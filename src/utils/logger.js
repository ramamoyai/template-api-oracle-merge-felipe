import winston from 'winston';
import httpContext from 'express-http-context';
import { v4 as uuidv4 } from 'uuid';
import config from './config.js';

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  debug: 4
};

if (config.LOG_COLORIZE) {
  const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'blue'
  };

  winston.addColors(colors);
}

// ?: extraer el formato de salida en la configuración?
const format = winston.format.combine(
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
  config.LOG_COLORIZE ? winston.format.colorize({ all: true }) : winston.format.uncolorize(),
  winston.format.printf((info) => `[${info.timestamp}] [${process.pid}] [${httpContext.get('uuid') || uuidv4()}] ${info.level}: ${info.message}`)
);

const transports = [
  new winston.transports.Console({
    format
  })
];

if (config.LOG_FILE_ENABLED) {
  transports.push(
    new winston.transports.File({
      format,
      filename: `logs/${config.LOG_FILE_NAME}` || 'logs/app.log',
      maxsize: config.LOG_FILE_MAX_SIZE || 52428800, // tamaño de rotado expresado en bytes
      maxFiles: config.LOG_FILE_MAX_FILES || 10 // mantiene los últimos maxFiles
    })
  );
}

const logger = winston.createLogger({
  level: config.LOG_LEVEL || 'http',
  levels,
  transports
});

export const stream = {
  write: (mensaje) => logger.http(mensaje)
};

export default logger;
import { almacenarEjemplo, obtenerEjemplos, obtenerEjemplo } from '../database.js';
import { catchDbError } from '../utils/dbErrorsUtil.js';
import { ErrorHttp } from '../utils/errors.js';

export const obtenerEjemplosController = async (req, res, next) => {
  try {
    const ejemplos = await obtenerEjemplos();
    res.status(200).json(ejemplos);
  } catch (err) {
    catchDbError(err, next);
  }
};

export const guardarNuevoEjemploController = async (req, res, next) => {
  const { body } = req;
  try {
    await almacenarEjemplo(body);
    res.status(201).json({ codigo: 'OK', mensaje: 'Ejemplo guardado exitosamente' });
  } catch (err) {
    catchDbError(err, next);
  }
};

export const obtenerEjemploController = async (req, res, next) => {
  try {
    const ejemplo = await obtenerEjemplo(req.params.campoClave);
    if (!ejemplo) return next(new ErrorHttp('ERROR_EJEMPLO_NO_ENCONTRADO'));
    res.status(200).json(ejemplo);
  } catch (err) {
    catchDbError(err, next);
  }
};

import { test } from '../database.js';
import { ErrorHttp } from '../utils/errors.js';

export const getReadyController = async (req, res, next) => {
  try {
    await test();
    res.status(200).json({
      codigo: 'OK',
      mensaje: 'Aplicación lista para operar'
    });
  } catch (err) {
    return next(new ErrorHttp('ERROR_TEST_BD'));
  }
};
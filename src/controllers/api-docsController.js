import path from 'path';
import { fileURLToPath } from 'url';
import YAML from 'yamljs';
import swaggerUi from 'swagger-ui-express';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const swaggerDocument = YAML.load(path.join(__dirname, '../resources/openapi.yaml'));

const options = {
  customCss: '.swagger-ui .topbar { display: none }'
};

export const prepareOpenAPI = (req, res, next) => {
  // Armo URL endpoint
  swaggerDocument.servers[0].variables.urlBase.default = `${req.get('X-Forwarded-Proto') || req.protocol}://${req.get('host')}`;
  // Seteo la versión del package.json
  swaggerDocument.info.version = process.env.npm_package_version;
  req.swaggerDoc = swaggerDocument;
  next();
};

export const setupOpenAPI = swaggerUi.setup(swaggerDocument, options);

export const serveOpenAPI = swaggerUi.serve;

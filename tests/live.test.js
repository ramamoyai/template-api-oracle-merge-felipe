import { request } from './utils/helpers.js';
import config from '../src/utils/config.js';

describe('Las solicitudes a live', () => {
  test('Devuelven ok si la API esta corriendo', async () => {
    const res = await request.get(`/${config.APP_NAME}/v1/live`).send();

    const expectedBody = {
      codigo: 'OK',
      mensaje: 'Aplicación en ejecución',
    };

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(expectedBody);
  });
});

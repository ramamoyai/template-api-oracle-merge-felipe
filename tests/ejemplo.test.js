import config from '../src/utils/config.js';
import { cerrarPool, instanciarSequelize, limpiarTabla, obtenerEjemplo, eliminarTabla } from '../src/database.js';
import { ejemplo1, ejemplo2, ejemplo3, ejemploErroneo } from './fixtures/ejemploFixture.js';
import { request, crearTablaAuxiliar, almacenarEjemploFixture } from './utils/helpers.js';

beforeAll(async () => {
  await instanciarSequelize();
  await crearTablaAuxiliar();
});

// TODO: reemplazar por tus casos de prueba
describe('La creacion de un ejemplo', () => {
  beforeEach(async () => {
    await limpiarTabla();
  });

  test('Deberia retornar OK si todos los datos son correctos', async () => {
    const res = await request.post(`/${config.APP_NAME}/v1/ejemplo`).send(ejemplo2);
    const documento = await obtenerEjemplo(ejemplo2.campoClave);
    const expectedBody = {
      codigo: 'OK',
      mensaje: 'Ejemplo guardado exitosamente',
    };
    expect(res.statusCode).toBe(201);
    expect(res.body).toEqual(expectedBody);
    expect(documento).toEqual(ejemplo2);
  });

  test('Deberia retornar Bad Request si no se envian parametros', async () => {
    const res = await request.post(`/${config.APP_NAME}/v1/ejemplo`).send({});
    expect(res.statusCode).toBe(400);
  });

  test('Deberia retornar Bad Request si uno o mas campos son incorrectos', async () => {
    const res = await request.post(`/${config.APP_NAME}/v1/ejemplo`).send(ejemploErroneo);
    const expectedBody = {
      codigo: 'ERROR_BODY',
      mensaje:
        '"campoClave" debe ser alfanumérico. "campoAlfanumerico" debe ser alfanumérico. "campoEntero" debe ser entero. "campoDecimal" debe ser numérico. "campoFecha" debe ser una fecha válida en formato ISO 8601. "campoTimeStamp" debe ser una fecha válida en formato ISO 8601',
    };

    expect(res.statusCode).toBe(400);
    expect(res.body).toEqual(expectedBody);
  });
});

// TODO: reemplazar por tus casos de prueba
describe('La obtencion de ejemplos', () => {
  beforeAll(async () => {
    await almacenarEjemploFixture(ejemplo1);
    await almacenarEjemploFixture(ejemplo2);
  });

  test('Deberia retornar OK con los ejemplos almacenados', async () => {
    const res = await request.get(`/${config.APP_NAME}/v1/ejemplo`).send();

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(expect.arrayContaining([ejemplo1, ejemplo2]));
  });
});

// TODO: reemplazar por tus casos de prueba
describe('La obtencion de un ejemplo', () => {
  beforeAll(async () => {
    await almacenarEjemploFixture(ejemplo3);
  });

  test('Deberia retornar OK con el ejemplo almacenado', async () => {
    const res = await request.get(`/${config.APP_NAME}/v1/ejemplo/${ejemplo3.campoClave}`).send();

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(ejemplo3);
  });
});

afterAll(async () => {
  await eliminarTabla();
  await cerrarPool();
});

// TODO: reemplazar por tus fixtures

export const ejemplo1 = {
  campoClave: 'clave1',
  campoAlfanumerico: 'Ejemplo',
  campoEntero: 2,
  campoDecimal: 1,
  campoFecha: '2021-10-04T00:00:00.000-03:00',
  campoTimeStamp: '2021-10-04T01:01:01.666-03:00',
};

export const ejemplo2 = {
  campoClave: 'clave2',
  campoAlfanumerico: 'Ejemplo2',
  campoEntero: 90,
  campoDecimal: 1.19,
  campoFecha: '2021-05-02T00:00:00.000-03:00',
  campoTimeStamp: '2022-01-01T01:01:01.123-03:00',
};

export const ejemplo3 = {
  campoClave: 'clave3',
  campoAlfanumerico: 'Ejemplo3',
  campoEntero: 33,
  campoDecimal: 333.33,
  campoFecha: '2022-01-02T00:00:00.000-03:00',
  campoTimeStamp: '2022-01-01T01:01:01.432-03:00',
};

export const ejemploErroneo = {
  campoClave: 1,
  campoAlfanumerico: 123,
  campoEntero: 3.2,
  campoDecimal: 'Hola',
  campoFecha: 'str',
  campoTimeStamp: 1,
};

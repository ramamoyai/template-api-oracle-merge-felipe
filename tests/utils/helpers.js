import app from '../../src/app.js';
import { crearTabla, almacenarEjemplo } from '../../src/database.js';
import supertest from 'supertest';
import { v4 as uuidv4 } from 'uuid';

//Generacion de requests de supertest con la api
export const request = supertest(app);

export const crearTablaAuxiliar = () => crearTabla(`AUX_${uuidv4().replace(/-/g, '_')}`);

export const almacenarEjemploFixture = (ejemplo) =>
  almacenarEjemplo({
    ...ejemplo,
    campoFecha: new Date(ejemplo.campoFecha),
    campoTimeStamp: new Date(ejemplo.campoTimeStamp),
  });

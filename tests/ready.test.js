import config from '../src/utils/config.js';
import { request } from './utils/helpers.js';
import { crearPool, cerrarPool } from '../src/database.js';

beforeAll(async () => {
  await crearPool();
});

describe('Las solicitudes a ready', () => {
  test('Devuelven ok si la API esta conectada a la base de datos', async () => {
    const res = await request.get(`/${config.APP_NAME}/v1/ready`).send();

    const expectedBody = {
      codigo: 'OK',
      mensaje: 'Aplicación lista para operar',
    };

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(expectedBody);
  });
});

afterAll(async () => {
  await cerrarPool();
});

# Template API Oracle

# Descripción de este template

Solo a fines prácticos y sin brindar ninguna utilidad real, se expones una api funcionando que va a servir de base para construir servicios similares. Para ver detalles sobre sobre la API, podés mirar la definción el OAS3.

## Caso de uso para el cual aplica

Aplica a un caso genérico en el cual:

- Se expone una operación a través de la cual el cliente hace una consulta (GET) y se le da una repuesta
- La base de datos consultada tiene un motor Oracle.
- Se ejecuta una sentencia SQL "pelada" (un SELECT o la ejecución de un PROCDURE).
- Se recibe el resultado.

Dependiendo de cuánto coincida esta descripción con el caso de uso para el cual vas a usar el template, vas a tener que tocar más o menos. Lo que sigue es una guía para que tenas en cuenta qué cosas vas a tener que tocar ante una problemática similar a la que acabamos de describir.

Se inlcuye el armado de la imagen docker.

## Features incluidos

- Logueo por pantalla, con nivel de log configurable
- Validación de los parámetros del request (uso de joi)
- Exposición de OAS3
- Funcionalidad de "live" y "ready" para control de salud
- Configuración a través de variable de ambientes
- Manejo de pool de conexiones

## Software de base

En el ambiente donde corra la aplicación se deberá tener instalado:

- **node** 18 (se recomienda la versión más acutal, que es la 18.16.1)
- **oraclient** 19 (se puede bajar de https://www.oracle.com/ar/database/technologies/instant-client/downloads.html)

# Cómo ejecuto levanto la APP

Así como está, el template se puede levantar directamente. Expone funcionalidades muy pavas, con el fin de "tener algo andando, ya de fábrica".

## En producción

    npm start

## En modo desarrollo

    npm run dev

## Ejecutar el linter

    npm run lint

Analiza el código en busca de problemas.

# Definición del servicio

La especificación [openapi.yaml](./src/resources/openapi.yaml) describe en forma completa en base a la especifiación de OAS3.

# Configuraciones

## Variables de Entorno

Se utilizan variables de entorno del sistema operativo donde se ejecuta la aplicación.

### Requeridas:

- Cliente Oracle:

        NLS_LANG=SPANISH_ARGENTINA.WE8MSWIN1252

- Contenedor:

        TZ=America/Argentina/Buenos_Aires

- Conexión a la bd:

        BD_HOST=localhost
        BD_PORT=1234
        BD_USER=user
        BD_PASSWORD=password
        BD_SERVICE_NAME=service
        BD_SCHEMA=schema

### Alternativas:

- Cambiar el puerto por defecto de la app:

        APP_PORT=5000

- Cambiar configuración de la conexión a la BD:

        BD_POOL_MAX=5
        BD_POOL_MIN=1
        BD_POOL_INCREMENT=1
        BD_POOL_QUEUE_TIMEOUT=5000
        BD_POOL_DRAIN_TIME=5
        BD_QUERY_TIMEOUT=5000

- Cambiar el comportamiento del logger:

        LOG_LEVEL=http
        LOG_HTTP_FORMAT=":method :url HTTP/:http-version :status :res[content-length] :response-time ms", req-headers::req-headers, req-body::req-body, res-body::res-body
        LOG_COLORIZE=true
        LOG_FILE_ENABLED=true
        LOG_FILE_NAME=app.log
        LOG_FILE_MAX_SIZE=52428800
        LOG_FILE_MAX_FILES=10

Para facilitar la configuración de las variables de entorno se puede definir un archivo .env en el directorio raiz de la app (se utiliza el módulo _dotenv_ para volcar la configuración al entorno).

**OJO**: este archivo no se sube al repo porque puede contener datos sensibles.

Consultar el archivo [default.env](default.env) el cuál especifica todas las variables de entorno contemplados por la app y sus valores por defecto (se utiliza el módulo _dotenv_ para volcar la configuración por defecto al entorno).

**_Importante_**: El orden de prevalencia de las variables de entorno es el siguiente (de mayor a menor):

1. Las Variables definidas originalmente en el entorno del sistema operativo.
2. Las variables configuradas en el archivo test.env (solo si se ejecuta npm test).
3. Las variables configuradas en el archivo .env
4. Las variables configuradas en el archivo default.env

Esto quiere decir que una vez definida una variable por primera vez (en el órden antes especificado) no será sobreescrita por ninguna configuración.

# Pipeline

Este proyecto se encuentra subido al pipeline de CI/CD.

## Webhooks

Actualmente el webhook de Tekton configurado en el proyecto de Gitlab es http://wk.agil.movistar.com.ar/nodejs-custom-pl

Este webhook se dispara a partir del "merge" de un MR en las ramas develop y release.

Realizar todas las tareas de CI/CD exceptuando el versionado de la aplicación y el deploy en cada ambiente.

## Imagen de Base

En el archivo [imagename](pipelines/baseimage/imagename) se encuentra la imagen que utilizará el pipeline para ejecutar las tareas de CI.

## Dockerfile

En el archivo [Dockerfile](pipelines/docker/Dockerfile) se encuentran los pasos de ejecución que utilizará el pipeline para construir la imagen de docker.

## Harbor

Harbor es la registry de docker donde quedará pusheada la imagen.

La URL para este proyecto quedaría formateada de la siguiente forma (siguiendo la misma estructura de Gitlab):

        <registry>/<directorio-gitlab>/<rama>/<campo-name-del-package>:<campo-tag-del-package>

Por ejemplo para develop:

        harbor.agil.movistar.com.ar/big-data/pipelines/big-data/microservices/templates/template-api-oracle/develop/template-api-oracle:1.7.0

Por ejemplo para release:

        harbor.agil.movistar.com.ar/big-data/pipelines/big-data/microservices/templates/template-api-oracle/release/template-api-oracle:1.7.0

**Nota**: Para master de momento el pipeline no ejecuta ninguna acción.
Idealmente si se realiza una certificación sobre el ambiente de test la imagen que debería estar referenciada en [values-prod.yaml](kubernetes/values-prod.yaml) es la de release.

## Versionado y MR

Es muy importante antes de realizar un MR de una feature contra develop asegurarse de incrementar de forma manual el versionado de la API en el archivo [package.json](package.json) y los archivos de values de cada ambiente [values-dev.yaml](kubernetes/values-dev.yaml), [values-test.yaml](kubernetes/values-test.yaml) y [values-prod.yaml](kubernetes/values-prod.yaml)

Esta tarea de momento queda manual hasta que sea automatizada por el pipeline.

**Importante**: de no realizar esta tarea el pipeline fallará y realizará un revert del merge efectuado por el MR en cuestión.

# Base de Datos

La definición de las tablas están en la carpeta assets\sql\*, ahí deben estar todos los DDL para referencia.

# Kubernetes

Se utiliza [Helm](https://helm.sh/) para la definición de los [templates](./kubernetes/templates), configuración y renederizado de los manifiestos de k8s.

En principio no será necesario modificar los templates. La tarea de configuración por cada servicio debe realizarse en los archivos de configuración que se detallan a continuación.

## Configuración

- [values.yaml](./kubernetes/values.yaml) => contiene la configuración por defecto.
- [values-dev.yaml](./kubernetes/values-dev.yaml) => contiene la configuración específica del ambiente de desarrollo.
- [values-test.yaml](./kubernetes/values-test.yaml) => contiene la configuración específica del ambiente de testing.
- [values-prod.yaml](./kubernetes/values-prod.yaml) => contiene la configuración específica del ambiente de producción.

**_Nota:_** En el archivo values.yaml solo se cargarán valores **_dummy_** en los secretos. Los valores **_reales_** serán pasados mediante argumentos en la línea de comandos al momento de renderizar los templates del ambiente.

## Verificar Templates

      helm lint ./kubernetes --values ./kubernetes/values-dev.yaml
      helm lint ./kubernetes --values ./kubernetes/values-test.yaml
      helm lint ./kubernetes --values ./kubernetes/values-prod.yaml

## Renderizar Templates

El proceso de renderizado es la generación de los manifiestos finales basados en la configuración pasada por parámetros.

      helm template ./kubernetes --values ./kubernetes/values-dev.yaml --set secret.BD_USER="<usuario-base64>" --set secret.BD_PASSWORD="<clave-base64>"
      helm template ./kubernetes --values ./kubernetes/values-test.yaml --set secret.BD_USER="<usuario-base64>" --set secret.BD_PASSWORD="<clave-base64>"
      helm template ./kubernetes --values ./kubernetes/values-prod.yaml --set secret.BD_USER="<usuario-base64>" --set secret.BD_PASSWORD="<clave-base64>"

**_Nota:_** en los ejemplos anteriores se muestran los comandos a ejecutar en la terminal para obtener en el sysout los manifiestos finales por cada ambiente. En el caso de los agentes de despliegue automático como **_ARGOCD_** tienen incorporados todos los plugins de manipulación de manifiestos y ejecuta estos comandos de forma automática al detectar cambios en las ramas que esté observando (develop, master, etc).

# Qué tengo que cambiar para adaptar el template

Acá te dejamos una guía con la idea de que el uso del template te resulte sencillo.

En lo que sea código, vas a encontrar comentarios que empiezan con "TODO:" ("To do", que son "cosas para hacer" en inglés).

## Agregar tus rutas

    ./src/routes/routes.js

Agregá las rutas que necesites, reemplazando la que se incluye como ejemplo (_template-consulta_)

## Agregá tu funcionalidad

Modificá el nombre y el contenido del archivo

    ./src/routes/ejemploRouter.js

El archivo se llama "ejemploRouter" porque en el template las entidades que se retornan en el response corresponden a una tabla de ejemplo. Vos tenés que cambiarle el nombre (y el coontenido) por el que que corresponda a lo que vas a retornar.

Acá está "la papa" de lo que vas a agregar. Cambiale el nombre por la entiadad que corresponda a lo que estás haciendo

## Aceso a base de datos

Acá vas a poder adecuar lo que corresponda en cuanto a qué sentencia ejecutar en la BD.

    ./src/database.js

## Agregar validaciones

    ./src/utils/validator.js

## Agregar errores (eventualmente)

Si tu API va a requerir errores no contemplados, hay que agregarlos acá:

    ./src/utils/errors.js

## Actualizar OAS3

Modificá el yaml para adaptarlo a la definición del servicio ¡No dejés rastros de que esto era un template! La limpieza influye en la calidad del entregable.

## Actualizar el README.md

Aplican las mismas consideraciones que expusimos par OAS3

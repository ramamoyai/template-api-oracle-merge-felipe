{{/*
helper.name genera el nombre de la aplicación
*/}}
{{- define "helper.name" -}}
{{ .Values.name }}
{{- end }}
{{/*
helper.name
*/}}
{{/*
helper.configName genera el nombre del configmap mediante un hash
*/}}
{{- define "helper.configName" -}}
{{ include "helper.name" . }}-{{ .Values.configmap | toString | adler32sum }}
{{- end }}
{{/*
helper.configName  genera el nombre del configmap mediante un hash
*/}}
{{/*
helper.secretName genera el nombre del secret mediante un hash
*/}}
{{- define "helper.secretName" -}}
{{ include "helper.name" . }}-{{ .Values.secret | toString | adler32sum }}
{{- end }}
{{/*
helper.secretName
*/}}
